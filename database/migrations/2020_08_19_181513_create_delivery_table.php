<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('delivery', function (Blueprint $table) {
      $table->id();
      $table->string('name_th');
      $table->string('name_en');
      $table->string('name_cn');
      $table->string('url')->nullable();
      $table->tinyInteger('active')->default(1);
      $table->integer('created_by');
      $table->integer('updated_by');
      $table->timestamps();
      $table->softDeletes('deleted_at', 0);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('delivery');
  }
}
