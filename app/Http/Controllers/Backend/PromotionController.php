<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Promotion;
use Illuminate\Support\Facades\Auth;

class PromotionController extends Controller
{
  const MODULE = 'promotion';

  public function index(Request $request) 
  {
    $this->authorize(mapPermission(self::MODULE));
    if ($request->filled('keyword')):
      $promotions = Promotion::getDataByKeyword($request->keyword)->get();
    else:
      $promotions = Promotion::limit(50)->get();
    endif;

    return view('backend.promotion.index', compact('promotions'));
  }

  public function search(Request $request) {
    $this->authorize(mapPermission(self::MODULE));
    if ($request->filled('keyword')):
      $promotions = Promotion::getDataByKeyword($request->keyword)->get();
    else:
      $promotions = Promotion::all();
    endif;

    return view('backend.promotion.show', compact('promotions'));
  }

  public function create() {
    $this->authorize(mapPermission(self::MODULE));
    $promotion = new Promotion;

    return view('backend.promotion.create', compact('promotion'));

  }

  public function store(Request $request) {
    $this->authorize(mapPermission(self::MODULE));
    $promotion = Promotion::create($this->validateRequest());
    $promotion->storeImage();

    return redirect(route('backend.promotion.index'));
  }

  public function edit(Promotion $promotion) {
    $this->authorize(mapPermission(self::MODULE));

    return view('backend.promotion.update', compact('promotion'));
  }

  public function update(Request $request, Promotion $promotion) {
    $this->authorize(mapPermission(self::MODULE));
    $promotion->update($this->validateRequest());
    $promotion->storeImage();

    return redirect(route('backend.promotion.index'));
  }

  public function destroy(Promotion $promotion) {
    $this->authorize(mapPermission(self::MODULE));
    $promotion->delete();

    return redirect(route('backend.promotion.index'));
  }

  private function validateRequest() {
    $validatedData = request()->validate([
      "title_th"   => "required",
      "title_en"   => "required",
      "title_cn"   => "required",
      "description_th" => "",
      "description_en" => "",
      "description_cn" => "",
      "detail_th" => "",
      "detail_en" => "",
      "detail_cn" => "",
      "start_date" => "",
      "end_date" => "",
      "active" => "",
    ]);

    request()->validate([
      "image"  => ['sometimes', 'file','image','max:5000'],
    ]);

    $validatedData['updated_by'] = Auth::id();
    if(request()->route()->getActionMethod() == 'store') :
      $validatedData['created_by'] = Auth::id();
    endif;

    return $validatedData;
  }

}

