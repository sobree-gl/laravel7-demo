<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\MenuAlacarte;
use App\Model\FoodType;
use Illuminate\Support\Facades\Auth;

class MenuAlacarteController extends Controller
{
  const MODULE = 'menu_alacarte';

  public function index(Request $request) 
  {
    $this->authorize(mapPermission(self::MODULE));
    if ($request->filled('keyword')):
      $menu_alacartes = MenuAlacarte::getDataByKeyword($request->keyword)->get();
    else:
      $menu_alacartes = MenuAlacarte::get();
    endif;

    return view('backend.menu_alacarte.index', compact('menu_alacartes'));
  }

  public function search(Request $request) {
    $this->authorize(mapPermission(self::MODULE));
    if ($request->filled('keyword')):
      $menu_alacartes = MenuAlacarte::getDataByKeyword($request->keyword)->get();
    else:
      $menu_alacartes = MenuAlacarte::all();
    endif;

    return view('backend.menu_alacarte.show', compact('menu_alacartes'));
  }

  public function create() {
    $this->authorize(mapPermission(self::MODULE));
    $menu_alacarte = new MenuAlacarte;
    $food_types = FoodType::onlyActive()->get();

    return view('backend.menu_alacarte.create', compact('menu_alacarte', 'food_types'));

  }

  public function store(Request $request) {
    $this->authorize(mapPermission(self::MODULE));
    $menu_alacarte = MenuAlacarte::create($this->validateRequest());
    $menu_alacarte->storeImage();

    return redirect(route('backend.menu_alacarte.index'));
  }

  public function edit(MenuAlacarte $menu_alacarte) {
    $this->authorize(mapPermission(self::MODULE));
    $food_types = FoodType::onlyActive()->get();

    return view('backend.menu_alacarte.update', compact('menu_alacarte', 'food_types'));
  }

  public function update(Request $request, MenuAlacarte $menu_alacarte) {
    $this->authorize(mapPermission(self::MODULE));
    $menu_alacarte->update($this->validateRequest());
    $menu_alacarte->storeImage();

    return redirect(route('backend.menu_alacarte.index'));
  }

  public function destroy(MenuAlacarte $menu_alacarte) {
    $this->authorize(mapPermission(self::MODULE));
    $menu_alacarte->delete();

    return redirect(route('backend.menu_alacarte.index'));
  }

  private function validateRequest() {
    $validatedData = request()->validate([
      "food_type_id"   => "required",
      "name_th"   => "required",
      "name_en"   => "required",
      "name_cn"   => "required",
      "description_th"   => "",
      "description_en"   => "",
      "description_cn"   => "",
      "price"   => "required",
      "recommended"   => "",
      "active" => "",
    ]);

    request()->validate([
      "image"  => ['sometimes', 'file','image','max:5000'],
    ]);

    $validatedData['updated_by'] = Auth::id();
    if(request()->route()->getActionMethod() == 'store') :
      $validatedData['created_by'] = Auth::id();
    endif;

    return $validatedData;
  }

}

