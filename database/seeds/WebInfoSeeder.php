<?php

use Illuminate\Database\Seeder;
use App\Model\WebInfo;

class WebInfoSeeder extends Seeder
{
  public function run()
  {
    WebInfo::create([
      'name_th' => 'Placeat est itaque ipsum occaecati quia tenetur alias.',
      'name_en' => 'Placeat est itaque ipsum occaecati quia tenetur alias.',
      'name_cn' => 'Placeat est itaque ipsum occaecati quia tenetur alias.',
      'created_by' => 1,
      'updated_by' => 1,
      'created_at' => NOW(),
      'updated_at' => NOW(),
    ]);

  }
}
