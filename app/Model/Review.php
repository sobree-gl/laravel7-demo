<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Review extends Model implements HasMedia
{
  use SoftDeletes, LogsActivity, HasMediaTrait;
  protected $table = 'reviews';
  protected $guarded = [];

  protected static $logName = 'reviews';
  protected static $logAttributes = ['*'];
  protected static $logOnlyDirty = true;
  protected $attributes = [
    'active' => 1,
  ];

  public function registerMediaCollections()
  {
    $this->addMediaCollection('image_user')->singleFile();
    $this->addMediaCollection('image')->singleFile();
  }

  public function storeImage()
  {
    if (request()->has('image_user')) {
      $this->addMediaFromRequest('image_user')
          ->sanitizingFileName(function($fileName) {
            return sanitizeFileName($fileName);
           })->toMediaCollection('image_user');
    }

    if (request()->has('image')) {
      $this->addMediaFromRequest('image')
          ->sanitizingFileName(function($fileName) {
            return sanitizeFileName($fileName);
           })->toMediaCollection('image');
    }
  }

  public function getImageUserAttribute()
  {
    return $this->getFirstMediaUrl('image_user');
  }

  public function getImageAttribute()
  {
    return $this->getFirstMediaUrl('image');
  }

  public function getImageDetailAttribute()
  {
    return $this->getMedia('image_detail');
  }

  public function getActiveAttribute($attributes) {
    return  [ 
      1 => 'Active' ,
      0 =>'Inactive'
    ][$attributes];
  }

  public function update_name() {
    return $this->hasOne('App\User', 'id', 'updated_by');
  }

  public function scopeonlyActive($query) {
    return $query->where('active', 1);
  }

  public function scopegetDataByKeyword($query, $keyword) {
    return $query->where('title_th', 'like', "%$keyword%")
      ->orWhere('title_en', 'like', "%$keyword%")
      ->orWhere('title_cn', 'like', "%$keyword%")
      ->orWhere('short_description_th', 'like', "%$keyword%")
      ->orWhere('short_description_en', 'like', "%$keyword%")
      ->orWhere('short_description_cn', 'like', "%$keyword%")
      ->orWhere('description_th', 'like', "%$keyword%")
      ->orWhere('description_en', 'like', "%$keyword%")
      ->orWhere('description_cn', 'like', "%$keyword%");
  }

}
