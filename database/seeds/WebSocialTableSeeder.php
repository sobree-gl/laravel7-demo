<?php

use Illuminate\Database\Seeder;

class WebSocialTableSeeder extends Seeder
{
  public function run()
  {
    \DB::table('web_social')->delete();
    
    \DB::table('web_social')->insert(array (
      0 => 
      array (
        'id' => 1,
        'name' => 'facebook',
        'url' => 'https://www.facebook.com/12345',
        'active' => 1,
        'created_by' => 1,
        'updated_by' => 1,
        'created_at' => NOW(),
        'updated_at' => NOW(),
      ),
      1 => 
      array (
        'id' => 2,
        'name' => 'instagram',
        'url' => 'https://www.instagram.com/12345',
        'active' => 1,
        'created_by' => 1,
        'updated_by' => 1,
        'created_at' => NOW(),
        'updated_at' => NOW(),
      ),
      2 => 
      array (
        'id' => 3,
        'name' => 'line',
        'url' => 'line://ti/p/@12345',
        'active' => 1,
        'created_by' => 1,
        'updated_by' => 1,
        'created_at' => NOW(),
        'updated_at' => NOW(),
      ),
      3 => 
      array (
        'id' => 4,
        'name' => 'youtube',
        'url' => '',
        'active' => 1,
        'created_by' => 1,
        'updated_by' => 1,
        'created_at' => NOW(),
        'updated_at' => NOW(),
      ),
    ));
  }
}