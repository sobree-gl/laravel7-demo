<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Facades\App\Repository\Pages;
use Facades\App\Repository\Banners;
use App\Model\Branch;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class BranchController extends Controller
{
  public function index()
  {
    $pages = Pages::get(7);
    $banners = Banners::get(7);
    $branchs = Branch::onlyActive()->get();

    return view('frontend.branch.index', compact(['pages', 'banners', 'branchs']));
  }

}

