@extends('frontend.layouts.main')

@section('title')
@endsection

<link href="{{ asset('css/frontend/menus.css?v') . time() }}" rel="stylesheet" />
<link href="{{ asset('plugins/fancyBox/source/jquery.fancybox.css?v=2.1.5') }}" rel="stylesheet" media="screen" />

@php
  $food_type = '';
@endphp

@section('content')
  <!-- begin #content -->
  <div id="content" class="content">
    <section class="box-banner-text">
      <div class="container">
        <div class="box-text">
					<h2 class="b-head" data-shadow="Buffet">{{ $pages->{get_lang('title')} }}</h2>
					<div class="quotationMarks" style="margin-left: 10%;">{{ __('messages.slogan') }} ... <t>Demo</t></div>
					<img src="{{ 'http://via.placeholder.com/150x150' }}" class="icon-premium">
        </div>
      </div>
    </section>
						
    <section class="box-tablist">
      <div class="container">
        <ul class="nav" role="tablist">
          @foreach($buffet_types as $buffet_type)
            <li role="presentation">
              <a href="#Buffet{{ $buffet_type->id }}" class="{{ $buffet_type->id == '1' ? 'active' : '' }}" aria-controls="Buffet{{ $buffet_type->id }}" role="tab" data-toggle="tab">
                <h3>{{ $buffet_type->{get_lang('name')} }}</h3>
                <b>{!! $buffet_type->{get_lang('description')} !!}</b>
              </a>
            </li>
          @endforeach
        </ul>
      </div>
    </section>
	        
    <div class="tab-content">
      @foreach($buffet_types as $buffet_type)
        <section class="box-List tab-pane {{ $buffet_type->id == '1' ? 'active' : '' }}" role="tabpanel" id="Buffet{{ $buffet_type->id }}">
          <div class="container">
            <div class="row">
              @foreach($buffet_menus as $buffet_menu)
                @if($buffet_menu->buffet_type_id <> $buffet_type->id)
                  @php
                    $food_type = '';
                  @endphp
                @endif
                @if($buffet_menu->buffet_type_id == $buffet_type->id)
                  @if($buffet_menu->food_type_id <> $food_type)
                    <div class="col-12">
                      <h4 class="pt-4">{{ !empty($buffet_menu->food_type) ? $buffet_menu->food_type->{get_lang('name')} : '' }}</h4>
                    </div>
                    @php
                      $food_type = $buffet_menu->food_type_id;
                    @endphp
                  @endif
                  <div class="col-lg-4 col-md-6 py-2">
                    <div class="item">
                      <a class="fancybox1" href="{{ $buffet_menu->image }}" data-fancybox-group="Buffet{{ $buffet_menu->buffet_type_id . '-' . $buffet_menu->food_type_id }}" title="{{ $buffet_menu->{get_lang('name')} }}">
                        <div class="img">
                          <div class="src-img" style="background-image: url({{ $buffet_menu->image }})">
                            <img src="{{ asset('images/size-img2.png') }}" alt=""><!-- ช่องนี้ห้ามแก้ -->
                          </div>
                        </div>
                        <p class="p-2">{{ $buffet_menu->{get_lang('name')} }}</p>
                      </a>
                    </div>
                  </div>
                @endif
              @endforeach
            </div>
            <h4 class="pt-4">{{ __('messages.detail') }}</h4>
            <div class="row">
              <div class="col-12">
                {!! $buffet_type->{get_lang('detail')} !!}
              </div>
            </div>
          </div>
        </section>
      @endforeach
    </div>
  </div>
  <!-- end #content -->
@endsection

@push('after-scripts')
  <script src="{{ asset('js/frontend/menus.js') }}"></script>
  <script src="{{ asset('plugins/fancyBox/source/jquery.fancybox.js?v=2.1.5') }}"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $(".fancybox1").fancybox({
        padding: 0 // remove white border
      });
    });
  </script>
@endpush
