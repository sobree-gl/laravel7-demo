@extends('frontend.layouts.main')

@section('title')
@endsection

<link href="{{ asset('css/frontend/about.css?v') . time() }}" rel="stylesheet" />

@section('content')
  <!-- begin #content -->
  <div id="content" class="content">
    <section class="box-banner BG-bottom-black">
      <h2 class="b-head" data-shadow="About">{{ $pages->{get_lang('title')} }}</h2>
      @foreach($banners as $banner)
        @if($banner->position == 1)
          @foreach($banner->banners_detail as $banner_detail)
            @switch($banner_detail->type)
              @case('image')
                <div class="BG-img" style="background-image: url('{{ $banner_detail->slide_banner_pc }}')"></div>
              @break
            @endswitch
          @endforeach
        @endif
      @endforeach
		</section>
			
    <section class="box-Ourstory BG-bottom">
      <div class="container width1500">
				<div class="row align-items-center">
					<div class="col-lg-6 col-md-12 px-4">
            <div class="b-text">
              <img src="{{ !empty($main['web_info']->logo_head) ? $main['web_info']->logo_head : 'http://via.placeholder.com/150x150' }}" class="icon-Logo">
              <h2 class="b-head t-color t-left">{{ $aboutus->{get_lang('title')} }}</h2>
							<p>{!! $aboutus->{get_lang('description1')} !!}</p>
						</div>
					</div>
  				<div class="col-lg-6 col-md-12 px-4">
  				  <img src="{{ !empty($aboutus->image1) ? $aboutus->image1 : 'http://via.placeholder.com/500x300' }}" class="img-Ourstory">
  				</div>
        </div>
      </div>
    </section>

    <section class="box-Ourstory2">
      <div class="container width1500">
        <div class="row">
          <div class="col-lg-6 col-md-12 px-4">
						<div class="b-text">
							<p>{!! $aboutus->{get_lang('description2')} !!}</p>
						</div>
					</div>
					<div class="col-lg-6 col-md-12 px-4">
						<img src="{{ !empty($aboutus->image2) ? $aboutus->image2 : 'http://via.placeholder.com/500x300' }}" class="img-Ourstory">
					</div>
				</div>
		  </div>
    </section>

    <section class="box-Ourstory3">
			<div class="container width1500">
				<div class="row">
					<div class="col-lg-6 col-md-12 px-4">
						<div class="b-text">
							<p>{!! $aboutus->{get_lang('description3')} !!}</p>
							<div class="quotationMarks">Placeat est itaque ipsum occaecati quia tenetur alias.</div>
							{{-- <img src="{{ asset('images/icon-premium.png') }}" class="icon-premium"> --}}
						</div>
					</div>
					<div class="col-lg-6 col-md-12 px-4">
						<img src="{{ !empty($aboutus->image3) ? $aboutus->image3 : 'http://via.placeholder.com/500x300' }}" class="img-Ourstory">
					</div>
				</div>
		  </div>
    </section>
	</div>
	<!-- end #content -->
@endsection

@push('after-scripts')
@endpush
