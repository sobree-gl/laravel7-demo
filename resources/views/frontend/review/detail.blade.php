@extends('frontend.layouts.main')

@section('title')
@endsection

@push('og')
  <meta property="og:title" content="{{ $review->{get_lang('title')} }}">
  <meta property="og:type" content="article">
  <meta property="og:url" content="{{ url()->current() }}">
  <meta property="og:image" content="{{ url($review->image) }}">
  <meta property="og:description" content="{!! $review->{get_lang('short_description')} !!}">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="{{ url()->current() }}">
  <meta name="twitter:creator" content="">
  <meta name="twitter:title" content="{{ $review->{get_lang('title')} }}">
  <meta name="twitter:image" content="{{ url($review->image) }}">
  <meta name="twitter:description" content="{!! $review->{get_lang('short_description')} !!}">
@endpush

<link href="{{ asset('css/frontend/detail.css?v') . time() }}" rel="stylesheet" />
<link href="{{ asset('plugins/fancyBox3/dist/jquery.fancybox.css?v=2.1.5') }}" rel="stylesheet" media="screen" />

<!-- GridGallery -->
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/GridGallery/css/component.css') }}" />

@section('content')
  <!-- begin #content -->
  <div id="content" class="content">
    <section class="box-detail">
      <div class="container">
        <div class="b-profile">
          <img src="{{ !empty($review->image_user) ? $review->image_user : 'http://via.placeholder.com/80x80' }}">
          <div class="b-name">
            <h5>{{ $review->{get_lang('title')} }}</h5>
            <div class="rateyo" data-rateyo-rating="{{ $review->point * 10 }}%"></div>
          </div>
        </div>
        <div class="box-deta">{{ $review->created_at->format('d F Y') }} | view {{ number_format($views) }}</div>
        <div class="p">
          <h3>{{ $review->{get_lang('short_description')} }}</h3>
				  {!! $review->{get_lang('description')} !!}
        </div>
      </div>
    </section>
	        
    <div id="grid-gallery" class="grid-gallery item-more container">
      <section class="grid-wrap">
        <ul class="grid">
          <li class="grid-sizer"></li><!-- for Masonry column width -->
          @if(!empty($review->image_detail))
            @foreach($review->image_detail as $image_detail)
              <li>
                <figure>
                  <a class="fancybox" href="{{ $image_detail->getUrl() }}" data-fancybox="images-preview" data-thumbs='{"autoStart":true}'>
                    <div class="img">
                    <img src="{{$image_detail->getUrl()}}" alt=""/>
                    </div>
                  </a>
                </figure>
              </li>
            @endforeach
          @endif
        </ul>
      </section>
		</div>
			
		<section class="box-social">
			<div class="container">
				<div class="b-social">
					<span>แชร์ไปยัง : </span>
					<ul class="social">
				    <li>
              <a class="I-fb" href="{{ get_link_share_facebook() }}" target="_blank" rel="noopener" aria-label="facebook"><img src="{{ asset('images/icon-fb.png') }}"></a>
            </li>
            <li>
              <a class="I-tw" href="{{ get_link_share_twitter( $review->{get_lang('title')} ) }}" target="_blank" rel="noopener" aria-label="twitter"><img src="{{ asset('images/icon-tw.png') }}"></a>
            </li>
					</ul>
				</div>
			</div>
		</section>
  </div>
  <!-- end #content -->
		
@endsection

@push('after-scripts')
<!-- fancybox3 -->
<script src="{{ asset('plugins/fancyBox3/dist/jquery.fancybox.min.js') }}"></script>

<!-- GridGallery -->
<script src="{{ asset('plugins/GridGallery/js/modernizr.custom.js') }}"></script>
<script src="{{ asset('plugins/GridGallery/js/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('plugins/GridGallery/js/masonry.pkgd.min.js') }}"></script>
<script src="{{ asset('plugins/GridGallery/js/cbpGridGallery.js') }}"></script>

<script>
	$(document).ready(function() {
		new CBPGridGallery( document.getElementById('grid-gallery') );
	});
</script>

@endpush
