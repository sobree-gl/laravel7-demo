		<!-- begin #sidebar -->
		<div id="sidebar" class="sidebar">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
			
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<ul class="nav">
					<li class="nav-header"></li>
          
          <li class="has-sub {{ request()->routeIs('backend.profile*') ? 'active' : '' }}">
            <a href="javascript:;">
              <b class="caret"></b>
              <i class="fa fa-user"></i>
              <span>ข้อมูลส่วนตัว</span>
            </a>
            <ul class="sub-menu">
              <li class="{{ request()->routeIs('backend.profile*') ? 'active' : '' }}">
                <a href="{{ route('backend.profile.index') }}" class="menu-link">โปรไฟล์</a>
              </li>
        
            </ul>
          </li>
					@canany (['access role', 'access user'])
						<li class="has-sub {{ request()->routeIs('backend.role*', 'backend.user*')  ? 'active' : '' }}">
							<a href="javascript:;">
					      <b class="caret"></b>
						    <i class="fas fa-cogs"></i>
						    <span>Settings</span>
							</a>
							<ul class="sub-menu">
                @can('access role')
                  <li class="{{ request()->routeIs('backend.role*') ? 'active' : '' }}">
                    <a href="{{ route('backend.role.index') }}" class="menu-link">สิทธิ์การเข้าใช้งาน</a>
                  </li>
                @endcan
                @can('access user')
                  <li class="{{ request()->routeIs('backend.user*') ? 'active' : '' }}">
                    <a href="{{ route('backend.user.index') }}" class="menu-link">ผู้ใช้งาน</a>
                  </li>
                @endcan               
							</ul>
						</li>
          @endcanany

          @canany (['access websoical', 'access webinfo', 'menu_front', 'access page'])
            <li class="has-sub {{ request()->routeIs('backend.websocial*', 'backend.webinfo*', 'backend.menu*', 'backend.page*')  ? 'active' : '' }}">
              <a href="javascript:;">
                <b class="caret"></b>
                <i class="fas fa-home"></i>
                <span>ข้อมูลหลัก</span>
              </a>

              <ul class="sub-menu">
                @can('access banner')
                  <li class="{{ request()->routeIs('backend.banner*') ? 'active' : '' }}">
                    <a href="{{ route('backend.banner.index') }}" class="menu-link">แบนเนอร์</a>
                  </li>
                @endcan
                @can('access webinfo')
                  <li class="{{ request()->routeIs('backend.webinfo*') ? 'active' : '' }}">
                    <a href="{{ route('backend.webinfo.index') }}" class="menu-link">ข้อมูลเว็บไซต์</a>
                  </li>
                @endcan
                @can('access websocial')
                  <li class="{{ request()->routeIs('backend.websocial*') ? 'active' : '' }}">
                    <a href="{{ route('backend.websocial.index') }}" class="menu-link">ข้อมูลโซเชี่ยล</a>
                  </li>
                @endcan
                @can('menu_front')
                  <li class="{{ request()->routeIs('backend.menu*') ? 'active' : '' }}">
                    <a href="{{ route('backend.menu.index') }}" class="menu-link">เมนูหน้าบ้าน</a>
                  </li>
                @endcan
                @can('access page')
                  <li class="{{ request()->routeIs('backend.page*') ? 'active' : '' }}">
                    <a href="{{ route('backend.page.index') }}" class="menu-link">Page (SEO)</a>
                  </li>
                @endcan
              </ul>
            </li>
          @endcanany

          @canany (['access food_type', 'access buffet_type', 'access menu_buffet', 'access menu_alacarte'])
            <li class="has-sub {{ request()->routeIs('backend.buffet_type*', 'backend.menu_buffet*', 'backend.menu_alacarte*')  ? 'active' : '' }}">
              <a href="javascript:;">
                <b class="caret"></b>
                <i class="fas fa-utensils"></i>
                <span>เมนูอาหาร</span>
              </a>
              <ul class="sub-menu">
                @can('access food_type')
                  <li class="{{ request()->routeIs('backend.food_type*') ? 'active' : '' }}">
                    <a href="{{ route('backend.food_type.index') }}" class="menu-link">ประเภทอาหาร</a>
                  </li>
                @endcan
                <li class="has-sub {{ request()->routeIs('backend.buffet_type*', 'backend.menu_buffet*')  ? 'active' : '' }}">
                  <a href="javascript:;">
                    <b class="caret"></b>
                    <span>Buffet</span>
                  </a>
                  <ul class="sub-menu">
                    @can('access buffet_type')
                      <li class="{{ request()->routeIs('backend.buffet_type*') ? 'active' : '' }}">
                        <a href="{{ route('backend.buffet_type.index') }}" class="menu-link">ประเภท</a>
                      </li>
                    @endcan
                    @can('access menu_buffet')
                      <li class="{{ request()->routeIs('backend.menu_buffet*') ? 'active' : '' }}">
                        <a href="{{ route('backend.menu_buffet.index') }}" class="menu-link">รายการ</a>
                      </li>
                    @endcan
                  </ul>
                </li>
                <li class="has-sub {{ request()->routeIs('backend.alacarte*', 'backend.menu_alacarte*')  ? 'active' : '' }}">
                  <a href="javascript:;">
                    <b class="caret"></b>
                    <span>Alacarte</span>
                  </a>
                  <ul class="sub-menu">
                    @can('access menu_alacarte')
                      <li class="{{ request()->routeIs('backend.alacarte*') ? 'active' : '' }}">
                        <a href="{{ route('backend.alacarte.index') }}" class="menu-link">รายละเอียด</a>
                      </li>
                    @endcan
                    @can('access menu_alacarte')
                      <li class="{{ request()->routeIs('backend.menu_alacarte*') ? 'active' : '' }}">
                        <a href="{{ route('backend.menu_alacarte.index') }}" class="menu-link">A la carte</a>
                      </li>
                    @endcan
                  </ul>
                </li>
                @can('access menu_delivery')
                  <li class="{{ request()->routeIs('backend.menu_delivery*') ? 'active' : '' }}">
                    <a href="{{ route('backend.menu_delivery.index') }}" class="menu-link">Delivery</a>
                  </li>
                @endcan
              </ul>
            </li>
          @endcan

          @can('access about')
            <li class="{{ request()->routeIs('backend.about*') ? 'active' : '' }}">
              <a href="{{ route('backend.about.index') }}" class="menu-link"><i class="fas fa-info"></i> <span>เกี่ยวกับเรา</span></a>
            </li>
          @endcan

          @can('access delivery')
            <li class="{{ request()->routeIs('backend.delivery*') ? 'active' : '' }}">
              <a href="{{ route('backend.delivery.index') }}" class="menu-link"><i class="fas fa-shipping-fast"></i> <span>บริการส่งอาหาร</span></a>
            </li>
          @endcan

          @can('access promotion')
            <li class="{{ request()->routeIs('backend.promotion*') ? 'active' : '' }}">
              <a href="{{ route('backend.promotion.index') }}" class="menu-link"><i class="fas fa-gift"></i> <span>โปรโมชั่น</span></a>
            </li>
          @endcan

          @can('access branch')
            <li class="{{ request()->routeIs('backend.branch*') ? 'active' : '' }}">
              <a href="{{ route('backend.branch.index') }}" class="menu-link"><i class="fas fa-code-branch"></i> <span>สาขา</span></a>
            </li>
          @endcan
         
          @can('access review')
            <li class="{{ request()->routeIs('backend.review*') ? 'active' : '' }}">
              <a href="{{ route('backend.review.index') }}" class="menu-link"><i class="fas fa-comment-alt"></i> <span>รีวิว</span></a>
            </li>
          @endcan

          @can('access gallery')
            <li class="{{ request()->routeIs('backend.gallery*') ? 'active' : '' }}">
              <a href="{{ route('backend.gallery.index') }}" class="menu-link"><i class="fas fa-images"></i> <span>แกลลอรี่</span></a>
            </li>
          @endcan

          @can('access contactus')
            <li class="{{ request()->routeIs('backend.contact*') ? 'active' : '' }}">
              <a href="{{ route('backend.contact.index') }}" class="menu-link"><i class="fas fa-question-circle"></i> <span>ติดต่อเรา</span></a>
            </li>
          @endcan
   
          @can('access trash')
  				  <li class="{{ request()->routeIs('backend.trash*') ? 'active' : '' }}">
              <a href="{{ route('backend.trash.index') }}" class="menu-link"><i class="fas fa-trash-alt"></i> <span>Trash</span></a>
            </li>
          @endcan
			    <!-- begin sidebar minify button -->
					<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
			        <!-- end sidebar minify button -->
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		<div class="sidebar-bg"></div>
		<!-- end #sidebar -->
