<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\FoodType;
use Illuminate\Support\Facades\Auth;

class FoodTypeController extends Controller
{
  const MODULE = 'food_type';

  public function index(Request $request) 
  {
    $this->authorize(mapPermission(self::MODULE));
    if ($request->filled('keyword')):
      $food_types = FoodType::getDataByKeyword($request->keyword)->get();
    else:
      $food_types = FoodType::limit(50)->get();
    endif;

    return view('backend.food_type.index', compact('food_types'));
  }

  public function search(Request $request) {
    $this->authorize(mapPermission(self::MODULE));
    if ($request->filled('keyword')):
      $food_types = FoodType::getDataByKeyword($request->keyword)->get();
    else:
      $food_types = FoodType::all();
    endif;

    return view('backend.food_type.show', compact('food_types'));
  }

  public function create() {
    $this->authorize(mapPermission(self::MODULE));
    $food_type = new FoodType;

    return view('backend.food_type.create', compact('food_type'));

  }

  public function store(Request $request) {
    $this->authorize(mapPermission(self::MODULE));
    $food_type = FoodType::create($this->validateRequest());
    $food_type->storeImage();

    return redirect(route('backend.food_type.index'));
  }

  public function edit(FoodType $food_type) {
    $this->authorize(mapPermission(self::MODULE));

    return view('backend.food_type.update', compact('food_type'));
  }

  public function update(Request $request, FoodType $food_type) {
    $this->authorize(mapPermission(self::MODULE));
    $food_type->update($this->validateRequest());
    $food_type->storeImage();

    return redirect(route('backend.food_type.index'));
  }

  public function destroy(FoodType $food_type) {
    $this->authorize(mapPermission(self::MODULE));
    $food_type->delete();

    return redirect(route('backend.food_type.index'));
  }

  private function validateRequest() {
    $validatedData = request()->validate([
      "name_th"   => "required",
      "name_en"   => "required",
      "name_cn"   => "required",
      "active" => "",
    ]);

    request()->validate([
      "image"  => ['sometimes', 'file','image','max:1000'],
    ]);

    $validatedData['updated_by'] = Auth::id();
    if(request()->route()->getActionMethod() == 'store') :
      $validatedData['created_by'] = Auth::id();
    endif;

    return $validatedData;
  }

}

