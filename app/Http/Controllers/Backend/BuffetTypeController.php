<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\BuffetType;
use Illuminate\Support\Facades\Auth;

class BuffetTypeController extends Controller
{
  const MODULE = 'buffet_type';

  public function index(Request $request) 
  {
    $this->authorize(mapPermission(self::MODULE));
    if ($request->filled('keyword')):
      $buffet_types = BuffetType::getDataByKeyword($request->keyword)->get();
    else:
      $buffet_types = BuffetType::limit(50)->get();
    endif;

    return view('backend.buffet_type.index', compact('buffet_types'));
  }

  public function search(Request $request) {
    $this->authorize(mapPermission(self::MODULE));
    if ($request->filled('keyword')):
      $buffet_types = BuffetType::getDataByKeyword($request->keyword)->get();
    else:
      $buffet_types = BuffetType::all();
    endif;

    return view('backend.buffet_type.show', compact('buffet_types'));
  }

  public function create() {
    $this->authorize(mapPermission(self::MODULE));
    $buffet_type = new BuffetType;

    return view('backend.buffet_type.create', compact('buffet_type'));

  }

  public function store(Request $request) {
    $this->authorize(mapPermission(self::MODULE));
    $buffet_type = BuffetType::create($this->validateRequest());
    $buffet_type->storeImage();

    return redirect(route('backend.buffet_type.index'));
  }

  public function edit(BuffetType $buffet_type) {
    $this->authorize(mapPermission(self::MODULE));

    return view('backend.buffet_type.update', compact('buffet_type'));
  }

  public function update(Request $request, BuffetType $buffet_type) {
    $this->authorize(mapPermission(self::MODULE));
    $buffet_type->update($this->validateRequest());
    $buffet_type->storeImage();

    return redirect(route('backend.buffet_type.index'));
  }

  public function destroy(BuffetType $buffet_type) {
    $this->authorize(mapPermission(self::MODULE));
    $buffet_type->delete();

    return redirect(route('backend.buffet_type.index'));
  }

  private function validateRequest() {
    $validatedData = request()->validate([
      "name_th"   => "required",
      "name_en"   => "required",
      "name_cn"   => "required",
      "description_th" => "",
      "description_en" => "",
      "description_cn" => "",
      "detail_th" => "",
      "detail_en" => "",
      "detail_cn" => "",
      "price"    => "required",
      "active" => "",
    ]);

    request()->validate([
      "image"  => ['sometimes', 'file','image','max:5000'],
    ]);

    $validatedData['updated_by'] = Auth::id();
    if(request()->route()->getActionMethod() == 'store') :
      $validatedData['created_by'] = Auth::id();
    endif;

    return $validatedData;
  }

}

