<?php

use App\Model\Branch;
use Illuminate\Support\Str;
use Faker\Generator as Faker;


$factory->define(Branch::class, function (Faker $faker) {
    return [
        'name_th' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'name_en' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'name_cn' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'address_th' => $faker->address(),
        'address_en' => $faker->address(),
        'address_cn' => $faker->address(),
        'telephone' => $faker->phoneNumber(),
        'office_hours' => '24 Hr.',
        'description_th' => $faker->country(),
        'description_en' => $faker->country(),
        'description_cn' => $faker->country(),
        'line' => '@'.$faker->cityPrefix(),
        'location' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15505.478464254014!2d100.6309176!3d13.696049849999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d608709779abf%3A0x90d1507ee2ccf4e9!2z4Lio4Li54LiZ4Lii4LmM4LiB4Li14Lis4Liy4LiX4Liy4LiH4LiZ4LmJ4LizIOC4muC4tuC4h-C4q-C4meC4reC4h-C4muC4reC4mQ!5e0!3m2!1sth!2sth!4v1612237859195!5m2!1sth!2sth" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>',
        'created_by' => 1,
        'updated_by' => 1,
        'created_at' => NOW(),
        'updated_at' => NOW(),
    ];
});
