<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // General User
        Role::create(['name' => 'general user']);

        // Admin
        $adminRole = Role::create(['name' => 'admin']);
        
        // Super Admin
        Role::create(['name' => 'super admin']);

        // Permissions
        $existPermissions = Permission::all()->pluck('name')->toArray();

        foreach($this->permissionLists() as $permission) {
            if (!in_array($permission, $existPermissions)) {
                Permission::create(['name' => $permission])->assignRole('admin');
            }
        }

        $adminRole->givePermissionTo(Permission::all());
    }

    public function permissionLists()
    {
      return [
        'access menu_front',
        'manage menu_front',
        'update menu_front',

        'access role',
        'manage role',
        'add role',
        'edit role',
        'delete role',

        'access user',
        'manage user',
        'add user',
        'edit user',
        'delete user',

        'access banner',
        'manage banner',
        'add banner',
        'edit banner',
        'delete banner',

        'access webinfo',
        'manage webinfo',
        'edit webinfo',
        
        'access websocial',
        'manage websocial',
        'add websocial',
        'edit websocial',
        'delete websocial',

        'access page',
        'manage page',
        'add page',
        'edit page',
        'delete page',

        'access about',
        'manage about',
        'edit about',

        'access food_type',
        'manage food_type',
        'add food_type',
        'edit food_type',
        'delete food_type',

        'access buffet_type',
        'manage buffet_type',
        'add buffet_type',
        'edit buffet_type',
        'delete buffet_type',

        'access menu_buffet',
        'manage menu_buffet',
        'add menu_buffet',
        'edit menu_buffet',
        'delete menu_buffet',

        'access menu_alacarte',
        'manage menu_alacarte',
        'add menu_alacarte',
        'edit menu_alacarte',
        'delete menu_alacarte',

        'access menu_delivery',
        'manage menu_delivery',
        'add menu_delivery',
        'edit menu_delivery',
        'delete menu_delivery',

        'access delivery',
        'manage delivery',
        'add delivery',
        'edit delivery',
        'delete delivery',

        'access review',
        'manage review',
        'add review',
        'edit review',
        'delete review',

        'access branch',
        'manage branch',
        'add branch',
        'edit branch',
        'delete branch',

        'access contactus',
        'manage contactus',
        'add contactus',
        'edit contactus',
        'delete contactus',

        'access gallery',
        'manage gallery',
        'edit gallery',
     
        'access trash',
        'manage trash',
        'remove trash',
        'restore trash',
        'restore_all trash',
        'remove_all trash',
      ];
    }
}