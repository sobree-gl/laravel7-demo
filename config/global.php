<?php

return [

  // setting global data
  'site_title' => 'Demo',
  'pagination' => 12, //item per page
  'day_check_new_item' => 7, //days
  'time_duration' => 60, //minutes

];
