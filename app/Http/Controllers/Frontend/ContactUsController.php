<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Facades\App\Repository\Pages;
use Facades\App\Repository\Banners;
use App\Model\ContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class ContactUsController extends Controller
{
  public function index()
  {
    $pages = Pages::get(8);
    $banners = Banners::get(8);
    $contactus = ContactUs::find(1);

    return view('frontend.contact.index', compact(['pages', 'banners', 'contactus']));
  }

  public function store($locale='', Request $request) {
    $contactus = ContactUs::create($this->validateRequest());
    $contactus->storeImage();
    $message = __('messages.contact_thankyou');
    $request->session()->flash('message', $message);
    $request->session()->flash('alert-class', 'alert-success');

    return redirect(route('frontend.contact', ['locale' => get_lang()]));
  }

  private function validateRequest() {
    $validatedData = request()->validate([
      "fullname"   => "required",
      "email"   => "required",
      "telephone"   => "required",
      "subject"   => "required",
      "detail"   => "required",
    ]);

    request()->validate([
      "image"  => ['sometimes', 'file','image','max:5000'],
    ]);

    $validatedData['updated_by'] = '1';
    if(request()->route()->getActionMethod() == 'store') :
      $validatedData['created_by'] = '1';
    endif;

    return $validatedData;
  }

}

