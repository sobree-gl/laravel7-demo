<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
  public function up()
  {
    Schema::create('reviews', function (Blueprint $table) {
      $table->id();
      $table->string('type', 100)->comment('customer, blogger, youtube')->default('customer')->nullable();
      $table->string('title_th');
      $table->string('title_en');
      $table->string('title_cn');
      $table->string('short_description_th');
      $table->string('short_description_en');
      $table->string('short_description_cn');
      $table->text('description_th');
      $table->text('description_en');
      $table->text('description_cn');
      $table->tinyInteger('point')->default(10);
      $table->tinyInteger('active')->default(1);
      $table->integer('created_by');
      $table->integer('updated_by');
      $table->timestamps();
      $table->softDeletes('deleted_at', 0);
    });
  }

  public function down()
  {
    Schema::dropIfExists('reviews');
  }
}
