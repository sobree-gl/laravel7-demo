<?php

use Illuminate\Database\Seeder;
use App\Model\Promotion;

class PromotionSeeder extends Seeder
{
  public function run()
  {
    Promotion::truncate();
    factory(Promotion::class, 20)->create();

    // Promotion::create([
    //   'title_th' => 'ทดสอบ',
    //   'title_en' => 'Test',
    //   'title_cn' => 'Test',
    //   'start_date' => NOW(),
    //   'end_date' => NOW(),
    //   'created_by' => 1,
    //   'updated_by' => 1,
    //   'created_at' => NOW(),
    //   'updated_at' => NOW(),
    // ]);
    
  }
}
