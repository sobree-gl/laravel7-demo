<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Model\WebInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WebInfoController extends Controller
{
  const MODULE = 'webinfo';

  public function index()
  {
    $this->authorize(mapPermission(self::MODULE));
    $webinfo = WebInfo::find(1)->first();

    return view('backend.webinfo.update', compact('webinfo'));
  }

  public function update(Request $request, WebInfo $webinfo)
  {
    $this->authorize(mapPermission(self::MODULE));
    $webinfo->update($this->validateRequest());
    $webinfo->storeImage();

    return redirect(route('backend.webinfo.index'));
  }

  private function validateRequest() {
    $validatedData = request()->validate([
      "name_th"               => "required",
      "name_en"               => "required",
      "name_cn"               => "required",
      "description_th"        => "required",
      "description_en"        => "required",
      "description_cn"        => "required",
      "copyright_th"          => "required",
      "copyright_en"          => "required",
      "copyright_cn"          => "required",
      "company_name_th"       => "required",
      "company_name_en"       => "required",
      "company_name_cn"       => "required",
      "company_address_th"    => "required",
      "company_address_en"    => "required",
      "company_address_cn"    => "required",
      "company_tax_code"      => "",
      "company_email"         => ["email","required"],
      "company_tel"           => "",
      "company_fax"           => "",
      "company_gmap_location" => "",
    ]);

    $validatedData['updated_by'] = Auth::id();
    $validateImg = request()->validate([
      "image_logo_head"       => ['sometimes', 'file','image','max:500'],
      "image_logo_foot"       => ['sometimes', 'file','image','max:500'],
      "image_flag_th"         => ['sometimes', 'file','image','max:200'],
      "image_flag_en"         => ['sometimes', 'file','image','max:200'],
      "image_flag_cn"         => ['sometimes', 'file','image','max:200'],
      "image_alacarte"        => ['sometimes', 'file','image','max:5000'],
      "image_buffet"          => ['sometimes', 'file','image','max:5000'],
      "image_delivery"        => ['sometimes', 'file','image','max:5000'],
    ]);

    return $validatedData;
  }

}

