<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Facades\App\Repository\Pages;
use Facades\App\Repository\Banners;
use App\Model\BuffetType;
use App\Model\FoodType;
use App\Model\MenuBuffet;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class BuffetMenuController extends Controller
{
  public function index()
  {
    $pages = Pages::get(10);
    $banners = Banners::get(10);
    $buffet_types = BuffetType::onlyActive()->get();
    $buffet_menus = MenuBuffet::join('food_type', 'food_type.id', '=', 'menu_buffet.food_type_id')
      ->orderBy('buffet_type_id', 'asc')->orderBy('food_type.sort_no', 'asc')->orderBy('menu_buffet.id')->select('menu_buffet.*')->get();

    return view('frontend.menus.buffet', compact(['pages', 'banners', 'buffet_types', 'buffet_menus']));
  }

}

