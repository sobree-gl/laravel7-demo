  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="fullname">ชื่อ <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="fullname" name="fullname" value="{{ old('fullname') ?? $contact->fullname }}" required="" />
          {{ $errors->first('fullname') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="email">อีเมล <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="email" name="email" value="{{ old('email') ?? $contact->email }}" required="" />
          {{ $errors->first('email') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="telephone">เบอร์โทร <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="telephone" name="telephone" value="{{ old('telephone') ?? $contact->telephone }}" required="" />
          {{ $errors->first('telephone') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="subject">เรื่องที่ต้องการติดต่อ <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="subject" name="subject" value="{{ old('subject') ?? $contact->subject }}" required="" />
          {{ $errors->first('subject') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="detail">รายละเอียด <span class="text-danger"> * </span> : </label>
          <textarea name="detail" class="form-control" id="detail" name="detail" rows="4" required="">{{  old('detail') ?? $contact->detail }}</textarea>
          {{ $errors->first('detail') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-12 mt-2">
          <label class="col-form-label" style='width: 100%;'>สถานะการตรวจสอบ : </label>         
          @if(empty($contact) || $contact->status == '0')
            <div class="radio radio-css radio-inline">
              <input class="form-check-input" type="radio" name="status" id="status1" value="1" >
              <label class="form-check-label ml-2" for="status1">ตรวจสอบแล้ว</label>
            </div>
            <div class="radio radio-css radio-inline">
              <input class="form-check-input" type="radio" name="status" id="status2" value="0" checked>
              <label class="form-check-label ml-2" for="status2">รอตรวจสอบ</label>
            </div>
          @else
            <div class="radio radio-css radio-inline">
              <input class="form-check-input" type="radio" name="status" id="status1" value="1" checked>
              <label class="form-check-label ml-2" for="status1">ตรวจสอบแล้ว</label>
            </div>
            <div class="radio radio-css radio-inline">
              <input class="form-check-input" type="radio" name="status" id="status2" value="0" >
              <label class="form-check-label ml-2" for="status2">รอตรวจสอบ</label>
            </div>         
          @endif
        </div>
      </div>

    </div> <!-- col6 -->
  </div> <!-- row -->
  <hr>
  <div class="form-group row mt-2">
    <div class="col-12 text-left">
      <button type="submit" class="btn btn-white"><i class="fa fa-save text-success"></i> บันทึกข้อมูล</button>
      <button type="reset" class="btn btn-white reset"><i class="fas fa-eraser text-warning"></i> ล้างข้อมูล</button>
      <button type="button" class="btn btn-white back" value="{{  url()->previous() }}"><i class="fas fa-reply text-danger" ></i> ย้อนกลับ</button>
    </div>
  </div>

@push('after-scripts')
  <script>

  $(function(){
    $('#form-validate').validate({
      errorPlacement: function(error, element) {
        if($(element).attr('id') == 'image'){
          error.insertAfter($(element).parent());
          $(element).siblings('.custom-file-label').toggleClass('error-border');
        } else {
          error.insertAfter(element);
        }
        
      }
    });

  });
  </script>
@endpush
       
