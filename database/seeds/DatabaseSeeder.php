<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    { 
      $this->call([
        MenusTableSeeder::class,
        RoleSeeder::class,
        UserSeeder::class,
        WebInfoSeeder::class,
        WebSocialTableSeeder::class,
        PagesTableSeeder::class,
        GallerySeeder::class,
        FoodTypeSeeder::class,
        BuffetTypeSeeder::class,
        
        ReviewSeeder::class,
        AboutUsSeeder::class,
        PromotionSeeder::class,
        BranchSeeder::class,
        DeliverySeeder::class,
        AlacarteSeeder::class,
        ]);

    }
}
