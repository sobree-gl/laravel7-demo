@extends('frontend.layouts.main')

@section('title')
@endsection

<link href="{{ asset('css/frontend/contact.css?v') . time() }}" rel="stylesheet" />

@section('content')
  <!-- begin #content -->
  <div id="content" class="content">
    <section class="box-banner BG-bottom-black">
      <h2 class="b-head" data-shadow="Contact us">{{ $pages->{get_lang('title')} }}</h2>
      @foreach($banners as $banner)
        @if($banner->position == 1)
          @foreach($banner->banners_detail as $banner_detail)
            @switch($banner_detail->type)
              @case('image')
                <div class="BG-img" style="background-image: url('{{ $banner_detail->slide_banner_pc }}')"></div>
              @break
            @endswitch
          @endforeach
        @endif
      @endforeach
    </section>

    <section class="box-form">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="b-text">
              <img src="{{ ($main['web_info']->logo_head) ? $main['web_info']->logo_head : 'http://via.placeholder.com/150x150' }}" class="icon-Logo">
              <h5>{{ $main['web_info']->{get_lang('company_name')} }}</h5>
              {!! $main['web_info']->{get_lang('company_address')} !!}
						</div>
					</div>
          <div class="col-md-6">
            <form class="form-horizontal" id="form-validate" name="demo-form" method="post" enctype="multipart/form-data" action="{{ route('frontend.contact.store', ['locale' => get_lang()]) }}">
              @method('post')
              @csrf
              <div class="form-group">
                <input type="text" class="form-control" name="fullname" placeholder="{{ __('messages.input_fullname') }}" required>
							</div>
              <div class="form-group">
							  <input type="email" class="form-control" name="email" placeholder="{{ __('messages.input_email') }}" required>
							</div>
              <div class="form-group">
							  <input type="tel" class="form-control" name="telephone" placeholder="{{ __('messages.input_telephone') }}" required>
							</div>
              <div class="form-group">
							  <input type="text" class="form-control" name="subject" placeholder="{{ __('messages.input_subject') }}" required>
							</div>
              <div class="form-group">
							  <textarea class="form-control" name="detail" rows="4" placeholder="{{ __('messages.input_detail') }}" required></textarea>
							</div>
							<div class="row">
                <div class="col-lg-5 col-md-12 col-sm-5">
	                <div class="input-file">
		                <input type="file" id="inputFile" name="image">
		                <a href="javascript:void(0)" onclick="$('#inputFile').click();">
			                <img src="{{ asset('images/icon-inputFile.png') }}">
			                {{ __('messages.browse') }}
			              </a>
                  </div>
	              </div>
                <div class="col">
									<div class="row">
                    <div class="col">
                      <button type="submit" class="btn2">{{ __('messages.send') }}</button>
                    </div>
                    <div class="col">
                      <button type="reset" class="btn2">{{ __('messages.reset') }}</button>
                    </div>
                  </div>
                </div>
              </div>
						</form>
            @if(Session::has('message'))
              <p id="success" class="alert {{ Session::get('alert-class', 'alert-light') }}">{{ Session::get('message') }}</p>
            @endif
          </div>
        </div>
      </div>
    </section>
	</div>
  <!-- end #content -->
@endsection

@push('after-scripts')
<script>
  $('#inputFile').change(function() {
    $('#inputFile').removeData('imageWidth');
    $('#inputFile').removeData('imageHeight');
    var file = this.files[0];
    var tmpImg = new Image();
    tmpImg.src=window.URL.createObjectURL( file ); 
    tmpImg.onload = function() {
      width = tmpImg.naturalWidth,
      height = tmpImg.naturalHeight;
      $('#inputFile').data('imageWidth', width);
      $('#inputFile').data('imageHeight', height);
    }
  });

  $.validator.addMethod('ImageMaxWidth', function(value, element, maxWidth) {
    if(element.files.length == 0){
      return true; // check here if file not added than return true for not check file dimention
    }
    var width = $(element).data('imageWidth');
    if(width <= maxWidth){
      return true;
    }else{
      return false;
    }
  });

  $(function(){
    $('#form-validate').validate({
      rules: {
        inputFile: {
          ImageMaxWidth: 900 //image max width 900 px
        }
      },
      messages: {
        inputFile: {
          ImageMaxWidth: "ความกว้างของรูปไม่เกิน 900 pixels"
        }
      },
      errorPlacement: function(error, element) {
        if($(element).attr('id') == 'inputFile'){
          error.insertAfter($(element).parent());
          $(element).siblings('.custom-file-label').toggleClass('error-border');
        } else {
          error.insertAfter(element);
        }
      }
    });

    setTimeout(function() { $("#success").hide(); }, 3000);
  });
</script>
@endpush
