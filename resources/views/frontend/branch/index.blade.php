@extends('frontend.layouts.main')

@section('title')
@endsection

<link href="{{ asset('css/frontend/branch.css?v') . time() }}" rel="stylesheet" />

@section('content')
  <!-- begin #content -->
  <div id="content" class="content">
    <section class="box-banner">
      <div class="item">
        @foreach($banners as $banner)
          @if($banner->position == 1)
            @foreach($banner->banners_detail as $banner_detail)
              @switch($banner_detail->type)
                @case('image')
                  <img class="img-banner banner-portrait" src="{{ $banner_detail->slide_banner_mobile }}">
                  <img class="img-banner" src="{{ $banner_detail->slide_banner_pc }}">
                @break
              @endswitch
            @endforeach
          @endif
        @endforeach
      </div>
    </section>
	        
    <section class="box-branch">
      <div class="container">
        <div class="box-head">
          <h2 class="b-head" data-shadow="Branch">{{ $pages->{get_lang('title')} }}</h2>
        </div>					
        <div class="row">
          @foreach($branchs as $branch)
            <div class="col-md-4 list">
              <img src="{{ ($branch->image) ? $branch->image : 'http://via.placeholder.com/500x300' }}" class="pb-3 d-none d-md-block">
              <div class="head-off-on">
                <b>Et consequatur accusamus est pariatur hic</b><br>
                <b class="I-map">{{ $branch->{get_lang('name')} }}</b>
              </div>
              <div class="text-off-on">
                <img src="{{ $branch->image }}" class="pb-3 d-block d-md-none">
                <p>{{ $branch->{get_lang('address')} }}</p>
                <p>{{ __('messages.tel_short') }} : {{ $branch->telephone }}</p>
                <p>{{ __('messages.office_hours') }} : {{ $branch->office_hours }}</p>
                <a href="{{ !empty($branch->line) ? $branch->line : '#' }}" target="_blank" class="btn-line">{{ __('messages.branch_contact') }}</a>
                <br><br>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </section>
  </div>		
  <!-- end #content -->
@endsection

@push('after-scripts')
@endpush
