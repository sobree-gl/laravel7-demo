<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\MenuBuffet;
use App\Model\BuffetType;
use App\Model\FoodType;
use Illuminate\Support\Facades\Auth;

class MenuBuffetController extends Controller
{
  const MODULE = 'menu_buffet';

  public function index(Request $request) 
  {
    $this->authorize(mapPermission(self::MODULE));
    if ($request->filled('keyword')):
      $menu_buffets = MenuBuffet::getDataByKeyword($request->keyword)->get();
    else:
      $menu_buffets = MenuBuffet::get();
    endif;

    return view('backend.menu_buffet.index', compact('menu_buffets'));
  }

  public function search(Request $request) {
    $this->authorize(mapPermission(self::MODULE));
    if ($request->filled('keyword')):
      $menu_buffets = MenuBuffet::getDataByKeyword($request->keyword)->get();
    else:
      $menu_buffets = MenuBuffet::all();
    endif;

    return view('backend.menu_buffet.show', compact('menu_buffets'));
  }

  public function create() {
    $this->authorize(mapPermission(self::MODULE));
    $menu_buffet = new MenuBuffet;
    $buffet_types = BuffetType::onlyActive()->get();
    $food_types = FoodType::onlyActive()->get();

    return view('backend.menu_buffet.create', compact('menu_buffet', 'buffet_types', 'food_types'));

  }

  public function store(Request $request) {
    $this->authorize(mapPermission(self::MODULE));
    $menu_buffet = MenuBuffet::create($this->validateRequest());
    $menu_buffet->storeImage();

    return redirect(route('backend.menu_buffet.index'));
  }

  public function edit(MenuBuffet $menu_buffet) {
    $this->authorize(mapPermission(self::MODULE));
    $buffet_types = BuffetType::onlyActive()->get();
    $food_types = FoodType::onlyActive()->get();

    return view('backend.menu_buffet.update', compact('menu_buffet', 'buffet_types', 'food_types'));
  }

  public function update(Request $request, MenuBuffet $menu_buffet) {
    $this->authorize(mapPermission(self::MODULE));
    $menu_buffet->update($this->validateRequest());
    $menu_buffet->storeImage();

    return redirect(route('backend.menu_buffet.index'));
  }

  public function destroy(MenuBuffet $menu_buffet) {
    $this->authorize(mapPermission(self::MODULE));
    $menu_buffet->delete();

    return redirect(route('backend.menu_buffet.index'));
  }

  private function validateRequest() {
    $validatedData = request()->validate([
      "buffet_type_id"   => "required",
      "food_type_id"   => "required",
      "name_th"   => "required",
      "name_en"   => "required",
      "name_cn"   => "required",
      "recommended"   => "",
      "active" => "",
    ]);

    request()->validate([
      "image"  => ['sometimes', 'file','image','max:5000'],
    ]);

    $validatedData['updated_by'] = Auth::id();
    if(request()->route()->getActionMethod() == 'store') :
      $validatedData['created_by'] = Auth::id();
    endif;

    return $validatedData;
  }

}

