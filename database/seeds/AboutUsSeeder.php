<?php

use Illuminate\Database\Seeder;
use App\Model\AboutUs;

class AboutUsSeeder extends Seeder
{
  public function run()
  {
    AboutUs::truncate();
    factory(AboutUs::class, 1)->create();

    // AboutUs::create([
    //   'title_th' => 'Our story',
    //   'title_en' => 'Our story',
    //   'title_cn' => 'Our story',
    //   'created_by' => 1,
    //   'updated_by' => 1,
    //   'created_at' => NOW(),
    //   'updated_at' => NOW(),
    // ]);

  }
}
