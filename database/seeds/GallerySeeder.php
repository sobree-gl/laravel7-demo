<?php

use Illuminate\Database\Seeder;
use App\Model\Gallery;

class GallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Gallery::create([
        'created_at' => NOW(),
        'updated_at' => NOW(),
      ]);
    }
}
