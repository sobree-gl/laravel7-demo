<?php

use Illuminate\Database\Seeder;
use App\Model\Branch;

class BranchSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    Branch::truncate();
    factory(Branch::class, 12)->create();

  }

}
