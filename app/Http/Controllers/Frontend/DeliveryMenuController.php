<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Facades\App\Repository\Pages;
use Facades\App\Repository\Banners;
use App\Model\MenuDelivery;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class DeliveryMenuController extends Controller
{
  public function index()
  {
    $pages = Pages::get(11);
    $banners = Banners::get(11);
    $delivery_menus = MenuDelivery::onlyActive()->orderBy('price', 'asc')->get();

    return view('frontend.menus.delivery', compact(['pages', 'banners', 'delivery_menus']));
  }

}

