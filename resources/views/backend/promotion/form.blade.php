  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="title_th">ไตเติ้ล (ไทย) <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="title_th" name="title_th" value="{{ old('title_th') ?? $promotion->title_th }}" required="" />
          {{ $errors->first('title_th') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="title_en">ไตเติ้ล (อังกฤษ) <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="title_en" name="title_en" value="{{ old('title_en') ?? $promotion->title_en }}" required="" />
          {{ $errors->first('title_en') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="title_cn">ไตเติ้ล (จีน) <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="title_cn" name="title_cn" value="{{ old('title_cn') ?? $promotion->title_cn }}" required="" />
          {{ $errors->first('title_cn') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="description_th">คำอธิบาย (ไทย) : </label>
          <textarea name="description_th" class="form-control text-count-150" id="description_th" name="description_th" rows="2">{{  old('description_th') ?? $promotion->description_th }}</textarea>
          {{ $errors->first('description_th') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="description_en">คำอธิบาย (อังกฤษ) : </label>
          <textarea name="description_en" class="form-control text-count-150" id="description_en" name="description_en" rows="2">{{  old('description_en') ?? $promotion->description_en }}</textarea>
          {{ $errors->first('description_en') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="description_cn">คำอธิบาย (จีน) : </label>
          <textarea name="description_cn" class="form-control text-count-150" id="description_cn" name="description_cn" rows="2">{{  old('description_cn') ?? $promotion->description_cn }}</textarea>
          {{ $errors->first('description_cn') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mt-2">
          <label class="" for="description_th">รายละเอียด (ไทย) : </label>
          <textarea name="detail_th" class="form-control" id="detail_th" name="detail_th" rows="2">{{  old('detail_th') ?? $promotion->detail_th }}</textarea>
          {{ $errors->first('detail_th') }}
        </div>

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mt-2">
          <label class="" for="detail_en">รายละเอียด (อังกฤษ) : </label>
          <textarea name="detail_en" class="form-control" id="detail_en" name="detail_en" rows="2">{{  old('detail_en') ?? $promotion->detail_en }}</textarea>
          {{ $errors->first('detail_en') }}
        </div>

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mt-2">
          <label class="" for="detail_cn">รายละเอียด (จีน) : </label>
          <textarea name="detail_cn" class="form-control" id="detail_cn" name="detail_cn" rows="2">{{  old('detail_cn') ?? $promotion->detail_cn }}</textarea>
          {{ $errors->first('detail_cn') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="start_date">วันที่เริ่มต้น <span class="text-danger"> * </span> : </label>
          <input type="date" class="form-control datepicker-startdate" id="" name="start_date" value="{{ old('start_date') ?? $promotion->start_date }}" required="" />
          {{ $errors->first('start_date') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="end_date">วันที่สิ้นสุด <span class="text-danger"> * </span> : </label>
          <input type="date" class="form-control datepicker-enddate" id="" name="end_date" value="{{ old('end_date') ?? $promotion->end_date }}" required="" />
          {{ $errors->first('end_date') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 mt-2">
          <label class="">รูป cover <span class="text-danger">*</span> :</label>
          <div class="icon">
            <div class="uploaded_image">
              <img id="preview_image" src="{{ $promotion->image ?? '' }}" class="img-icon" data-toggle="popover"  data-html="true"/>
            </div>
          </div>
          
          <div class="custom-file">
            <input type="file" 
             class="image" id="image" name="image" {{request()->route()->getActionMethod() == 'create' ? 'required' : ''}}>
            <label class="custom-file-label" for="image" >เลือกรูป</label>
           
          </div>
          <label class='text-pic'>ขนาดภาพที่แนะนำ 900x450 (ขนาดไม่เกิน 5 MB)</label>
            {{ $errors->first('image') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-6 col-md-12 col-lg-6">
          <label class="col-form-label" style='width: 100%;'>สถานะการใช้งาน : </label>         
          <div class="radio radio-css radio-inline Me-2">
            @if(!empty($promotion) && $promotion->active == 'Inactive') 
              <input class="form-check-input" type="radio" name="active" id="active1" value="1" >
              <label class="form-check-label ml-2" for="active1">เปิดใช้งาน</label>
            </div>
            <div class="radio radio-css radio-inline">
              <input class="form-check-input" type="radio" name="active" id="active2" value="0" checked>
              <label class="form-check-label ml-2" for="active2">ปิดใช้งาน</label>
            </div>
             
           @else
              <input class="form-check-input" type="radio" name="active" id="active1" value="1" checked>
              <label class="form-check-label ml-2" for="active1">เปิดใช้งาน</label>
            </div>
            <div class="radio radio-css radio-inline">
              <input class="form-check-input" type="radio" name="active" id="_IsActive2" value="0" >
              <label class="form-check-label ml-2" for="_IsActive2">ปิดใช้งาน</label>
            </div>         
          @endif
        </div>
      </div>

    </div> <!-- col6 -->
  </div> <!-- row -->
  <hr>
  <div class="form-group row mt-2">
    <div class="col-12 text-left">
      <button type="submit" class="btn btn-white"><i class="fa fa-save text-success"></i> บันทึกข้อมูล</button>
      <button type="reset" class="btn btn-white reset"><i class="fas fa-eraser text-warning"></i> ล้างข้อมูล</button>
      <button type="button" class="btn btn-white back" value="{{  url()->previous() }}"><i class="fas fa-reply text-danger" ></i> ย้อนกลับ</button>
    </div>
  </div>


@push('after-scripts')
  <script>
  $('#image').change(function() {
    $('#image').removeData('imageWidth');
    $('#image').removeData('imageHeight');
    var file = this.files[0];
    var tmpImg = new Image();
    tmpImg.src=window.URL.createObjectURL( file ); 
    tmpImg.onload = function() {
      width = tmpImg.naturalWidth,
      height = tmpImg.naturalHeight;
      $('#image').data('imageWidth', width);
      $('#image').data('imageHeight', height);
    }
  });

  $.validator.addMethod('ImageMaxWidth', function(value, element, maxWidth) {
    if(element.files.length == 0){
      return true; // check here if file not added than return true for not check file dimention
    }
    var width = $(element).data('imageWidth');
    if(width <= maxWidth){
      return true;
    }else{
      return false;
    }
  });

  $(function(){
    $('#form-validate').validate({
      rules: {
        image: {
          ImageMaxWidth: 900 //image max width 900 px
        }
      },
      messages: {
        image: {
          ImageMaxWidth: "ความกว้างของรูปไม่เกิน 900 pixels"
        }
      },
      errorPlacement: function(error, element) {
        if($(element).attr('id') == 'image'){
          error.insertAfter($(element).parent());
          $(element).siblings('.custom-file-label').toggleClass('error-border');
        } else {
          error.insertAfter(element);
        }
        
      }
    });

    $('#image').on('change', function(){
      readURL(this, "preview_image");
    });

    $('.text-count-150').characterCounter({
      minlength: 0,
      maxlength: 150,
      blockextra: true,
      position: 'top',
    });

    CKEDITOR.replace('detail_th', {
      filebrowserUploadUrl: "{{route('backend.ckeditor.upload', ['_token' => csrf_token() ])}}",
      filebrowserUploadMethod: 'form'
    });

    CKEDITOR.replace('detail_en', {
      filebrowserUploadUrl: "{{route('backend.ckeditor.upload', ['_token' => csrf_token() ])}}",
      filebrowserUploadMethod: 'form'
    });

    CKEDITOR.replace('detail_cn', {
      filebrowserUploadUrl: "{{route('backend.ckeditor.upload', ['_token' => csrf_token() ])}}",
      filebrowserUploadMethod: 'form'
    });
  });
  </script>
@endpush
       
