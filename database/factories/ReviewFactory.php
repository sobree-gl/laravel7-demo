<?php

use App\Model\Review;
use Illuminate\Support\Str;
use Faker\Generator as Faker;


$factory->define(Review::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement(array('blogger','customer','youtube')),
        'title_th' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'title_en' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'title_cn' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'short_description_th' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'short_description_en' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'short_description_cn' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'description_th' => $faker->text(),
        'description_en' => $faker->text(),
        'description_cn' => $faker->text(),
        'created_by' => '1',
        'updated_by' => '1',
    ];
});
