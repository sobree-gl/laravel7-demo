 <div class="row">
    <div class="col-md-12">
      <div class="row mt-2 ml-1 mr-1">
        <div class="col-md-12">
          <div class="row">
            <div class="col-xl-6 col-md-12 col-sm-12">
              <label class='col-form-label' for="name_th">ชื่อสาขาวิชา(ไทย) <span class='text-danger'> * </span> : </label>
              <input type="text" class="form-control " id="name_th" name="name_th" value="{{  old('name_th') ?? $department->name_th }}" required="" />
              {{ $errors->first('name_th') }}
            </div>

             <div class="col-xl-6 col-md-12 col-sm-12">
              <label class='col-form-label' for="name_en">ชื่อสาขาวิชา(อังกฤษ) :</label>
              <input type="text" class="form-control " id="name_en" name="name_en" value="{{  old('name_en') ?? $department->name_en }}" required/>
              {{ $errors->first('name_en') }}
            </div>
          </div>
        </div> 
      </div> 

    <div class="row mt-2 ml-1 mr-1">
      <div class="col-md-12">
        <div class="row">      
          <div class="col-xl-6 col-md-12 col-lg-6">   
            <label class='col-form-label' for="email">Email สาขา <span class='text-danger'> * </span> : </label>
            <input type="email" class="form-control " id="email" name="email" value="{{  old('email') ?? $department->email }}" />
               {{ $errors->first('email') }} 
          </div>

          <div class="col-xl-6 col-md-12 col-lg-6">          
            <label class="col-form-label" style='width: 100%;'>สถานะการใช้งาน : </label>         
            <div class="radio radio-css radio-inline Me-2">
              @if(!empty($department) && $department->active == 'Inactive') 
                <input class="form-check-input" type="radio" name="active" id="active1" value="1" >
                <label class="form-check-label ml-2" for="active1">เปิดใช้งาน</label>
              </div>
              <div class="radio radio-css radio-inline">
                <input class="form-check-input" type="radio" name="active" id="active2" value="0" checked>
                <label class="form-check-label ml-2" for="active2">ปิดใช้งาน</label>
              </div>
               
             @else
                <input class="form-check-input" type="radio" name="active" id="active1" value="1" checked>
                <label class="form-check-label ml-2" for="active1">เปิดใช้งาน</label>
              </div>
              <div class="radio radio-css radio-inline">
                <input class="form-check-input" type="radio" name="active" id="_IsActive2" value="0" >
                <label class="form-check-label ml-2" for="_IsActive2">ปิดใช้งาน</label>
              </div>         
            @endif
          </div>

          
        </div>
      </div>
    </div>


      <div class="row mt-2 ml-1 mr-1">
        <div class="col-md-12">
          <div class="row">      
            
            <div class="col-xl-6 col-md-12 col-sm-12 col-lg-6">
              <label class="">รูป Icon <span class='text-danger'>*</span> :</label>
              <div class="icon">  
                <div class="uploaded_image">  
                  <img id="preview_image_icon" src="{{ $department->image_icon ?? '' }}" class="img-icon"  data-toggle='popover'  data-html='true'/>
                </div>
              </div>
              <div class="custom-file">  
                <input type="file" 
                 class="image_icon" id="image_icon" name="image_icon">
                <label class="custom-file-label" for="image_icon" >เลือกรูป</label>
               
              </div>
                <label class='text-pic'>ขนาดภาพที่แนะนำ 100x100 (ขนาดไม่เกิน 0.2 MB)</label>
                {{ $errors->first('image_icon') }}
            </div>

            <div class="col-xl-6 col-md-12 col-sm-12 col-lg-6">
              <label class="">รูปสาขา <span class='text-danger'>*</span> :</label>
              <div class="icon">  
                <div class="uploaded_image">  
                  <img id="preview_image" src="{{ $department->image ?? '' }}" class="img-icon"  data-toggle='popover'  data-html='true'/>
                </div>
              </div>
              <div class="custom-file">  
                <input type="file" 
                 class="image" id="image" name="image">
                <label class="custom-file-label" for="image" >เลือกรูป</label>
               
              </div>
                <label class='text-pic'>ขนาดภาพที่แนะนำ 100x100 (ขนาดไม่เกิน 0.2 MB)</label>
                {{ $errors->first('image') }}
            </div>



           
          </div>
        </div>
      </div>

    </div> <!-- col6 -->
  </div> <!-- row -->
  <hr>
  <div class="form-group row mt-2">
    <div class="col-12 text-left">
      <button type="submit" class="btn btn-white"><i class="fa fa-save text-success"></i> บันทึกข้อมูล</button>
      <button type="reset" class="btn btn-white reset"><i class="fas fa-eraser text-warning"></i> ล้างข้อมูล</button>
      <button type="button" class="btn btn-white back" value="{{  url()->previous() }}"><i class="fas fa-reply text-danger" ></i> ย้อนกลับ</button>      
    </div>
  </div>


@push('after-scripts')
  <script>
    $(function(){
      $('#form-validate').validate({
        
       errorPlacement: function(error, element) {
        if($(element).attr('id') == 'image'){
          error.insertAfter($(element).parent());
          $(element).siblings('.custom-file-label').toggleClass('error-border');
        } else {
          error.insertAfter(element);
        }
        
      }
    });

    $('#image').on('change', function(){
     readURL(this, "preview_image");
    });

     $('#image_icon').on('change', function(){
     readURL(this, "preview_image_icon");
    });

    image_icon
  });
  </script>
@endpush
       
