 <div class="row">
    <div class="col-md-12">
      
      <div class="row mt-2 ml-1 mr-1">
        <div class="col-md-12">
          <div class="row">
            <div class="col-xl-6 col-md-12 col-sm-12">
              <label class='col-form-label' for="title_th">หัวข้อ(ไทย) <span class='text-danger'> * </span> : </label>
              <input type="text" class="form-control " id="title_th" name="title_th" value="{{  old('title_th') ?? $news->title_th }}" required="" />
              {{ $errors->first('title_th') }}
            </div>

             <div class="col-xl-6 col-md-12 col-sm-12">
              <label class='col-form-label' for="title_en">หัวข้อ(อังกฤษ) <span class='text-danger'> * </span> : </label>
              <input type="text" class="form-control " id="title_en" name="title_en" value="{{  old('title_en') ?? $news->title_en }}" required="" />
              {{ $errors->first('title_en') }}
            </div>
          </div>
        </div> 
      </div>  

      <div class="row mt-2 ml-1 mr-1">
        <div class="col-md-12">
          <div class="row">
            <div class="col-xl-6 col-md-12 col-sm-12">
              <label class='col-form-label' for="short_description_th">รายละเอียดอย่างสั้น(ไทย) <span class='text-danger'> * </span> : </label>
              <textarea name="short_description_th"  class="form-control " id="short_description_th" name="short_description_th"  required="" rows='2'>{{  old('short_description_th') ?? $news->short_description_th }}</textarea>
              {{ $errors->first('short_description_th') }}
            </div>

             <div class="col-xl-6 col-md-12 col-sm-12">
              <label class='col-form-label' for="short_description_en">รายละเอียดอย่างสั้น(อังกฤษ) <span class='text-danger'> * </span> : </label>
              <textarea name="short_description_en"  class="form-control " id="short_description_en" name="short_description_en"  required="" rows='2'>{{  old('short_description_en') ?? $news->short_description_en }}</textarea>
              {{ $errors->first('short_description_en') }}
            </div>
          </div>
        </div> 
      </div> 

      <div class="row mt-2 ml-1 mr-1">
        <div class="col-md-12">
          <div class="row">
            <div class="col-xl-6 col-md-12 col-sm-12">
              <label class='' for="description_th">รายละเอียด(ไทย) <span class='text-danger'> * </span> : </label>
              <textarea name="description_th"  class="form-control " id="description_th" name="description_th"  required="" rows='4'>{{  old('description_th') ?? $news->description_th }}</textarea>
              {{ $errors->first('description_th') }}
            </div>

             <div class="col-xl-6 col-md-12 col-sm-12">
              <label class='' for="description_en">รายละเอียด(อังกฤษ) <span class='text-danger'> * </span> : </label>
              <textarea name="description_en"  class="form-control " id="description_en" name="description_en"  required="" rows='4'>{{  old('description_en') ?? $news->description_en }}</textarea>
              {{ $errors->first('description_en') }}
            </div>
          </div>
        </div> 
      </div>

      <div class="row mt-2 ml-1 mr-1">
        <div class="col-md-12">
          <div class="row"> 
            <div class="col-xl-3 col-md-12 col-sm-12">
              <label class='col-form-label' for="title_en">ประเภท <span class='text-danger'> * </span> : </label>
              <select name="type" id="type" class='form-control '  >
                <option value="Off-Campus" {{ empty($news->type) || $news->type == 'Off-Campus' ? 'selected' : '' }}>Off-Campus</option>
                <option value="In-Campus" {{ !empty($news->type) && $news->type == 'In-Campus' ? 'selected' : '' }}>In-Campus</option>
              </select>
            </div>
          </div>
        </div>
      </div>

      <div class="row mt-2 ml-1 mr-1">
        <div class="col-md-12">
          <div class="row">      
            <div class="col-xl-6 col-md-12 col-sm-12 col-lg-6">
              <label class="">รูปข่าวสาร <span class='text-danger'>*</span> :</label>
              <div class="icon">  
                <div class="uploaded_image">  
                  <img id="preview_image" src="{{ $news->image ?? '' }}" class="img-icon"  data-toggle='popover'  data-html='true'/>
                </div>
              </div>
              
              <div class="custom-file">  
                <input type="file" 
                 class="image" id="image" name="image" {{request()->route()->getActionMethod() == 'create' ? 'required' : ''}}>
                <label class="custom-file-label" for="image" >เลือกรูป</label>
               
              </div>
              <label class='text-pic'>ขนาดภาพที่แนะนำ 100x100 (ขนาดไม่เกิน 0.2 MB)</label>
                {{ $errors->first('image') }}
            </div>

            <div class="col-xl-6 col-md-12 col-lg-6">          
              <label class="col-form-label" style='width: 100%;'>สถานะการใช้งาน : </label>         
              <div class="radio radio-css radio-inline Me-2">
                @if(!empty($news) && $news->active == 'Inactive') 
                  <input class="form-check-input" type="radio" name="active" id="active1" value="1" >
                  <label class="form-check-label ml-2" for="active1">เปิดใช้งาน</label>
                </div>
                <div class="radio radio-css radio-inline">
                  <input class="form-check-input" type="radio" name="active" id="active2" value="0" checked>
                  <label class="form-check-label ml-2" for="active2">ปิดใช้งาน</label>
                </div>
                 
               @else
                  <input class="form-check-input" type="radio" name="active" id="active1" value="1" checked>
                  <label class="form-check-label ml-2" for="active1">เปิดใช้งาน</label>
                </div>
                <div class="radio radio-css radio-inline">
                  <input class="form-check-input" type="radio" name="active" id="_IsActive2" value="0" >
                  <label class="form-check-label ml-2" for="_IsActive2">ปิดใช้งาน</label>
                </div>         
              @endif
            </div>
          </div>
        </div>
      </div>


      <div class="form-group mt-2">
        <label for="image_detail">รูปรายละเอียด</label>
        <div class="needsclick dropzone" id="document-dropzone">
        </div>
      </div>
     

    </div> <!-- col6 -->
  </div> <!-- row -->
  <hr>
  <div class="form-group row mt-2">
    <div class="col-12 text-left">
      <button type="submit" class="btn btn-white"><i class="fa fa-save text-success"></i> บันทึกข้อมูล</button>
      <button type="reset" class="btn btn-white reset"><i class="fas fa-eraser text-warning"></i> ล้างข้อมูล</button>
      <button type="button" class="btn btn-white back" value="{{  url()->previous() }}"><i class="fas fa-reply text-danger" ></i> ย้อนกลับ</button>      
    </div>
  </div>


@push('after-scripts')
  <script>
    // Dropzone.autoDiscover = false;
    $(function(){
      $('#form-validate').validate({
        
         errorPlacement: function(error, element) {
          if($(element).attr('id') == 'image'){
            error.insertAfter($(element).parent());
            $(element).siblings('.custom-file-label').toggleClass('error-border');
          } else {
            error.insertAfter(element);
          }
          
        }
      });
      CKEDITOR.replace('description_th', {
          filebrowserUploadUrl: "{{route('backend.ckeditor.upload', ['_token' => csrf_token() ])}}",
          filebrowserUploadMethod: 'form'
      });

      CKEDITOR.replace('description_en', {
          filebrowserUploadUrl: "{{route('backend.ckeditor.upload', ['_token' => csrf_token() ])}}",
          filebrowserUploadMethod: 'form'
      });

      $('#image').on('change', function(){
       readURL(this, "preview_image");
      });
    });

    var uploadedImageDetailMap = {}
    Dropzone.options.documentDropzone = {
      url: '{!! route('backend.dropzone.upload') !!}',
      maxFilesize: 2, // MB
      addRemoveLinks: true,
      headers: {
        'X-CSRF-TOKEN': "{{ csrf_token() }}"
      },
      success: function (file, response) {
        $('form').append('<input type="hidden" name="image_detail[]" value="' + response.name + '">')
        uploadedImageDetailMap[file.name] = response.name
      },
      removedfile: function (file) {
        file.previewElement.remove()
        var name = ''
        if (typeof file.file_name !== 'undefined') {
          name = file.file_name
        } else {
          name = uploadedDocumentMap[file.name]
        }
        $('form').find('input[name="image_detail[]"][value="' + name + '"]').remove()
      },
      init: function () {
        @if(isset($news) && $news->image_detail)
          var files =
            {!! json_encode($news->image_detail) !!}
          for (var i in files) {
            var file = files[i]
            this.options.addedfile.call(this, file)
            file.previewElement.classList.add('dz-complete')
            $('form').append('<input type="hidden" name="image_detail[]" value="' + file.file_name + '">')
          }
        @endif
      }
    }
  </script>
@endpush
       
