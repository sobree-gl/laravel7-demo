<!-- ================== BEGIN BASE JS ================== -->
<script src="{{ asset('plugins/jquery/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('plugins/jquery/jquery-migrate-1.1.0.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-validation/dist/jquery.validate.js') }}"></script>
<script src="{{ asset('plugins/jquery-validation/dist/localization/messages_th.js') }}"></script>
<!--[if lt IE 9]>
  <script src="../crossbrowserjs/html5shiv.js"></script>
  <script src="../crossbrowserjs/respond.min.js"></script>
  <script src="../crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="{{ asset('plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('plugins/js-cookie/js.cookie.js') }}"></script>
<script src="{{ asset('js/theme/default.min.js') }}"></script>
<script src="{{ asset('js/apps.js') }}"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<!-- Sweet Alert -->
<script src="{{ asset('plugins/bootstrap-sweetalert/sweetalert.min.js') }}"></script>
<!-- owl.carousel -->
<script src="{{ asset('plugins/owlcarousel/owl.carousel.js') }}"></script>
<!-- plugins starrating -->
<script src="{{ asset('plugins/starrating/jquery.rateyo.js') }}"></script>

@if (!empty($script['google_map'])) 
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
  <script src="{{ asset('js/demo/map-google.demo.min.js') }}"></script>
@endif

@if (!empty($script['xxx'])) 
@endif

<script src="{{ asset('js/frontend/script.js') }}"></script>

<script>
$(document).ready(function() {
  $(".rateyo").rateYo({
    spacing: "10px",
    starWidth: "15px",
    normalFill: "#c8c6c0",
    ratedFill: "#d9292b",
    numStars: 5,
    readOnly: true
  });
});
</script>

  <!-- ================== END PAGE LEVEL JS ================== -->
