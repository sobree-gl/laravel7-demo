<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Facades\App\Repository\Pages;
use Facades\App\Repository\Banners;
use App\Model\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class GalleryController extends Controller
{
  public function index()
  {
    $pages = Pages::get(6);
    $banners = Banners::get(6);
    $gallery = Gallery::find(1);

    return view('frontend.gallery.index', compact(['pages', 'banners', 'gallery']));
  }

}

