<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodMenusTable extends Migration
{
  public function up()
  {
    Schema::create('food_type', function (Blueprint $table) {
      $table->id();
      $table->string('name_th');
      $table->string('name_en');
      $table->string('name_cn');
      $table->tinyInteger('sort_no')->default(1);
      $table->tinyInteger('active')->default(1);
      $table->integer('created_by');
      $table->integer('updated_by');
      $table->timestamps();
      $table->softDeletes('deleted_at', 0);
    });

    Schema::create('buffet_type', function (Blueprint $table) {
      $table->id();
      $table->string('name_th');
      $table->string('name_en');
      $table->string('name_cn');
      $table->text('description_th')->nullable();
      $table->text('description_en')->nullable();
      $table->text('description_cn')->nullable();
      $table->integer('price')->default(0);
      $table->text('detail_th')->nullable();
      $table->text('detail_en')->nullable();
      $table->text('detail_cn')->nullable();
      $table->tinyInteger('active')->default(1);
      $table->integer('created_by');
      $table->integer('updated_by');
      $table->timestamps();
      $table->softDeletes('deleted_at', 0);
    });

    Schema::create('alacarte', function (Blueprint $table) {
      $table->id();
      $table->string('detail_th');
      $table->string('detail_en');
      $table->string('detail_cn');
      $table->tinyInteger('active')->default(1);
      $table->integer('created_by');
      $table->integer('updated_by');
      $table->timestamps();
      $table->softDeletes('deleted_at', 0);
    });

    Schema::create('menu_buffet', function (Blueprint $table) {
      $table->id();
      $table->integer('buffet_type_id');
      $table->integer('food_type_id');
      $table->string('name_th');
      $table->string('name_en');
      $table->string('name_cn');
      $table->tinyInteger('recommended')->default(0);
      $table->tinyInteger('active')->default(1);
      $table->integer('created_by');
      $table->integer('updated_by');
      $table->timestamps();
      $table->softDeletes('deleted_at', 0);
    });

    Schema::create('menu_alacarte', function (Blueprint $table) {
      $table->id();
      $table->integer('food_type_id');
      $table->string('name_th');
      $table->string('name_en');
      $table->string('name_cn');
      $table->text('description_th')->nullable();
      $table->text('description_en')->nullable();
      $table->text('description_cn')->nullable();
      $table->integer('price')->default(0);
      $table->tinyInteger('recommended')->default(0);
      $table->tinyInteger('active')->default(1);
      $table->integer('created_by');
      $table->integer('updated_by');
      $table->timestamps();
      $table->softDeletes('deleted_at', 0);
    });

    Schema::create('menu_delivery', function (Blueprint $table) {
      $table->id();
      $table->string('name_th');
      $table->string('name_en');
      $table->string('name_cn');
      $table->text('description_th')->nullable();
      $table->text('description_en')->nullable();
      $table->text('description_cn')->nullable();
      $table->integer('price')->default(0);
      $table->text('detail_th')->nullable();
      $table->text('detail_en')->nullable();
      $table->text('detail_cn')->nullable();
      $table->tinyInteger('recommended')->default(0);
      $table->tinyInteger('active')->default(1);
      $table->integer('created_by');
      $table->integer('updated_by');
      $table->timestamps();
      $table->softDeletes('deleted_at', 0);
    });
  }

  public function down()
  {
    Schema::dropIfExists('food_type');
    Schema::dropIfExists('buffet_type');
    Schema::dropIfExists('alacarte');
    Schema::dropIfExists('menu_buffet');
    Schema::dropIfExists('menu_alacarte');
    Schema::dropIfExists('menu_delivery');
  }
}
