<div class="container width90">
  <nav class="navbar navbar-expand-xl navbar-dark">
    <a class="navbar-brand" href="{{ route('frontend.home', ['locale' => get_lang()]) }}">
      <img src="{{ !empty($main['web_info']->logo_head) ? $main['web_info']->logo_head : 'http://via.placeholder.com/150x150' }}">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav ml-auto menu-list">
        @foreach($main['menus'] as $key => $menu)
          @if ($menu->pages->id == $pages->parent_id)
            @php $parent_menu_active = 'dropdown active';
            @endphp
          @else
            @php $parent_menu_active = 'dropdown';
            @endphp
          @endif
          @if ($menu->page_id == $pages->id)
            @php $menu_active = 'active';
            @endphp
          @else
            @php $menu_active = '';
            @endphp
          @endif
          <li id="{{ $menu->id }}" class="nav-item {{ collect($menu->childrens)->count() > 0 ? $parent_menu_active : $menu_active }}">
            <a href="{{ collect($menu->childrens)->count() > 0 ? '#' : route($menu->pages->route_name, ['locale' => get_lang()]) }}" class="nav-link {{ collect($menu->childrens)->count() > 0 ? 'dropdown-toggle' : '' }}"  data-toggle="{{ collect($menu->childrens)->count() > 0 ? 'dropdown' : '' }}">{{ $menu->{ get_lang('name')} }}</a>
            @if(collect($menu->childrens)->count() > 0)
              <div class="dropdown-menu">
                @foreach ($menu->childrens as $key => $child)
                  <a href="{{ collect($child->childrens)->count() > 0 ? '#' : route($child->pages->route_name, ['locale' => get_lang()]) }}" id="{{ $child->id }}" class="dropdown-item {{ $child->page_id == $pages->id ? 'active' : '' }}">{{ $child->{ get_lang('name') } }}</a>
                  </a>
                @endforeach
              </div>
            @endif
          </li>
        @endforeach
      </ul>
      <ul class="navbar-nav menu-social">
        @foreach($main['web_socials'] as $web_social)
          @if(!empty($web_social->url))
            <li>
              <a target="_blank" href="{{ $web_social->url }}" rel="noopener">
                <img src="{{ ($web_social->image) ? $web_social->image : 'http://via.placeholder.com/150x150' }}">
              </a>
            </li>
          @endif
        @endforeach
      </ul>
      <div class="dropdown box-lang">
        <button class="btn dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          @if (strtoupper(get_lang()) == 'TH')
            <img src="{{ ($main['web_info']->flag_th) ? $main['web_info']->flag_th : asset('images/I-TH.jpg') }}">
          @elseif (strtoupper(get_lang()) == 'EN')
            <img src="{{ ($main['web_info']->flag_en) ? $main['web_info']->flag_en : asset('images/I-EN.jpg') }}">
          @elseif (strtoupper(get_lang()) == 'CN')
            <img src="{{ ($main['web_info']->flag_cn) ? $main['web_info']->flag_cn : asset('images/I-CN.jpg') }}">
          @endif
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
          @php
            $lang = array('/th/', '/en/', '/cn/');
          @endphp
          <a href="{{ str_replace($lang, '/th/', request()->url()) }}"><button class="dropdown-item" type="button"><img src="{{ ($main['web_info']->flag_th) ? $main['web_info']->flag_th : asset('images/I-TH.jpg') }}"></button></a>
          <a href="{{ str_replace($lang, '/en/', request()->url()) }}"><button class="dropdown-item" type="button"><img src="{{ ($main['web_info']->flag_en) ? $main['web_info']->flag_en : asset('images/I-EN.jpg') }}"></button></a>
          <a href="{{ str_replace($lang, '/cn/', request()->url()) }}"><button class="dropdown-item" type="button"><img src="{{ ($main['web_info']->flag_cn) ? $main['web_info']->flag_cn : asset('images/I-CN.jpg') }}"></button></a>
        </div>
      </div>
    </div>
  </nav>
</div>
