  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="name_th">ชื่อผู้ให้บริการ (ไทย) <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="name_th" name="name_th" value="{{ old('name_th') ?? $delivery->name_th }}" required="" />
          {{ $errors->first('name_th') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="name_en">ชื่อผู้ให้บริการ (อังกฤษ) <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="name_en" name="name_en" value="{{ old('name_en') ?? $delivery->name_en }}" required="" />
          {{ $errors->first('name_en') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="name_cn">ชื่อผู้ให้บริการ (จีน) <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="name_cn" name="name_cn" value="{{ old('name_cn') ?? $delivery->name_cn }}" required="" />
          {{ $errors->first('name_cn') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="url">url <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="url" name="url" value="{{ old('url') ?? $delivery->url }}" placeholder="https://" />
          {{ $errors->first('url') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 mt-2">
          <label class="">รูป cover <span class="text-danger">*</span> :</label>
          <div class="icon">
            <div class="uploaded_image">
              <img id="preview_image" src="{{ $delivery->image ?? '' }}" class="img-icon" data-toggle="popover"  data-html="true"/>
            </div>
          </div>
          
          <div class="custom-file">
            <input type="file" 
             class="image" id="image" name="image" {{request()->route()->getActionMethod() == 'create' ? 'required' : ''}}>
            <label class="custom-file-label" for="image" >เลือกรูป</label>
          </div>
          <label class='text-pic'>ขนาดภาพที่แนะนำ 600x600 (ขนาดไม่เกิน 2 MB)</label>
          {{ $errors->first('image') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-6 col-md-12 col-lg-6">
          <label class="col-form-label" style='width: 100%;'>สถานะการใช้งาน : </label>         
          <div class="radio radio-css radio-inline Me-2">
            @if(!empty($delivery) && $delivery->active == 'Inactive') 
              <input class="form-check-input" type="radio" name="active" id="active1" value="1" >
              <label class="form-check-label ml-2" for="active1">เปิดใช้งาน</label>
            </div>
            <div class="radio radio-css radio-inline">
              <input class="form-check-input" type="radio" name="active" id="active2" value="0" checked>
              <label class="form-check-label ml-2" for="active2">ปิดใช้งาน</label>
            </div>
             
           @else
              <input class="form-check-input" type="radio" name="active" id="active1" value="1" checked>
              <label class="form-check-label ml-2" for="active1">เปิดใช้งาน</label>
            </div>
            <div class="radio radio-css radio-inline">
              <input class="form-check-input" type="radio" name="active" id="_IsActive2" value="0" >
              <label class="form-check-label ml-2" for="_IsActive2">ปิดใช้งาน</label>
            </div>         
          @endif
        </div>
      </div>

    </div> <!-- col6 -->
  </div> <!-- row -->
  <hr>
  <div class="form-group row mt-2">
    <div class="col-12 text-left">
      <button type="submit" class="btn btn-white"><i class="fa fa-save text-success"></i> บันทึกข้อมูล</button>
      <button type="reset" class="btn btn-white reset"><i class="fas fa-eraser text-warning"></i> ล้างข้อมูล</button>
      <button type="button" class="btn btn-white back" value="{{  url()->previous() }}"><i class="fas fa-reply text-danger" ></i> ย้อนกลับ</button>
    </div>
  </div>

@push('after-scripts')
  <script>
  $('#image').change(function() {
    $('#image').removeData('imageWidth');
    $('#image').removeData('imageHeight');
    var file = this.files[0];
    var tmpImg = new Image();
    tmpImg.src=window.URL.createObjectURL( file ); 
    tmpImg.onload = function() {
      width = tmpImg.naturalWidth,
      height = tmpImg.naturalHeight;
      $('#image').data('imageWidth', width);
      $('#image').data('imageHeight', height);
    }
  });

  $.validator.addMethod('ImageMaxWidth', function(value, element, maxWidth) {
    if(element.files.length == 0){
      return true; // check here if file not added than return true for not check file dimention
    }
    var width = $(element).data('imageWidth');
    if(width <= maxWidth){
      return true;
    }else{
      return false;
    }
  });

  $(function(){
    $('#form-validate').validate({
      rules: {
        image: {
          ImageMaxWidth: 600 //image max width 600 px
        }
      },
      messages: {
        image: {
          ImageMaxWidth: "ความกว้างของรูปไม่เกิน 600 pixels"
        }
      },
      errorPlacement: function(error, element) {
        if($(element).attr('id') == 'image'){
          error.insertAfter($(element).parent());
          $(element).siblings('.custom-file-label').toggleClass('error-border');
        } else {
          error.insertAfter(element);
        }
        
      }
    });

    $('#image').on('change', function(){
      readURL(this, "preview_image");
    });
  });
  </script>
@endpush
       
