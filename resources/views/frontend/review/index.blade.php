@extends('frontend.layouts.main')

@section('title')
@endsection

<link href="{{ asset('css/frontend/reviews.css?v') . time() }}" rel="stylesheet" />

@section('content')
  <!-- begin #content -->
  <div id="content" class="content">
    @if(count($banners)>0)
    <section class="box-banner">
      @foreach($banners as $banner)
        @if($banner->position == 1)
          @foreach($banner->banners_detail as $banner_detail)
            @switch($banner_detail->type)
              @case('image')
                <div class="BG-img" style="background-image: url('{{ $banner_detail->slide_banner_pc }}')"></div>
              @break
              @case('youtube')
                <div class="box-video container">
                  <iframe src="https://www.youtube.com/embed/{{$banner_detail->url}}?mute=1&autoplay=1&loop=1&playlist={{$banner_detail->url}}" frameborder="0"></iframe>
                </div>
                <div class="BG-img" style="background-image: url('http://img.youtube.com/vi/{{$banner_detail->url}}/maxresdefault.jpg');"></div>
              @break
              @case('facebook')
                <div class="box-video container">
                  <iframe src="https://www.facebook.com/plugins/video.php?autoplay=true&href=https%3A%2F%2Fwww.facebook.com%2FBestBeefSukhumvit%2Fvideos%2F{{ $banner_detail->url }}%2F&show_text=0&width=560" width="560" height="415" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
                </div>
              @break
            @endswitch
          @endforeach
        @endif
      @endforeach
    </section>
    @endempty

    <section class="box-List">
      <div class="container">
        <div class="box-head">
          <h2 class="b-head" data-shadow="Reviews">{{ $pages->{get_lang('title')} }}</h2>
        </div>
        <div class="row">
          @foreach($reviews as $review)
            <div class="col-md-4 item">
              <a href="{{ route('frontend.review-detail', ['locale' => get_lang(), 'review' => $review->id]) }}">
                <div class="img">
                  <div class="src-img" style="background-image: url({{ ($review->image) ? $review->image : 'http://via.placeholder.com/500x300' }})">
                    <img src="{{ asset('images/size-img2.png') }}" alt=""><!-- ช่องนี้ห้ามแก้ -->
                  </div>
                </div>
                <div class="b-profile">
                  <img src="{{ !empty($review->image_user) ? $review->image_user : 'http://via.placeholder.com/80x80' }}">
                  <div class="b-name">
                    <h5>{{ $review->{get_lang('title')} }}</h5>
                    <div class="rateyo" data-rateyo-rating="{{ $review->point * 10 }}%"></div>
                  </div>
                </div>
                <p>{{ $review->{get_lang('short_description')} }}</p>
              </a>
            </div>
          @endforeach
        </div>
      </div>
    </section>

    <section>
			{{ $reviews->withQueryString()->links('frontend.layouts.paginator') }}
    </section>
  </div>	
	<!-- end #content -->
@endsection

@push('after-scripts')
@endpush
