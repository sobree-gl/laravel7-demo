  <!-- ================== BEGIN BASE CSS STYLE ================== -->
  <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300;400&family=Kaushan+Script&display=swap" rel="stylesheet">

  <link href="{{ asset('plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('plugins/bootstrap/4.0.0/css/bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('plugins/font-awesome/5.0/css/fontawesome-all.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('css/frontend/custom_style.css?v') . time() }}" rel="stylesheet" />
  <link href="{{ asset('plugins/owlcarousel/owl.carousel.min.css') }}" rel="stylesheet" />
  <!-- plugins starrating -->
  <link href="{{ asset('plugins/starrating/jquery.rateyo.css') }}" rel="stylesheet" />
  <!-- ================== END BASE CSS STYLE ================== -->

  <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
  @if(!empty($css['xxx']))
    
  @endif
  <!-- ================== END PAGE LEVEL STYLE ================== -->
  
  <link href="{{ asset('css/frontend/custom_style.css?v') . time() }}" rel="stylesheet" />
