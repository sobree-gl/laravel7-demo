<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactusTable extends Migration
{
  public function up()
  {
    Schema::create('contact_us', function (Blueprint $table) {
      $table->id();
      $table->string('fullname');
      $table->string('email');
      $table->string('telephone');
      $table->string('subject');
      $table->text('detail');
      $table->tinyInteger('status')->default(0)->comment('0:อยู่ระหว่างดำเนินการ, 1:ดำเนินการแล้ว');
      $table->tinyInteger('active')->default(1);
      $table->integer('created_by');
      $table->integer('updated_by');
      $table->timestamps();
      $table->softDeletes('deleted_at', 0);
    });
  }

  public function down()
  {
    Schema::dropIfExists('contact_us');
  }
}
